import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun';
import NavigateNext from '@material-ui/icons/NavigateNext';

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  list: {
    width: '80%',
  },
  listItem: {
    background: '#fafafa',
    marginTop: '10px',
    borderRadius: '10px',
  }
});

function FolderList(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <List className={classes.list}>
        <ListItem className={classes.listItem}>
          <Avatar>
            <DirectionsRunIcon />
          </Avatar>
          <ListItemText primary="New York Marathon" secondary="Jan 9, 2018" />
            <ListItemSecondaryAction>
            <IconButton aria-label="Comments">
                <NavigateNext />
            </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Avatar>
            <DirectionsRunIcon />
          </Avatar>
          <ListItemText primary="Rome Half Marathon" secondary="Jan 7, 2018" />
          <ListItemSecondaryAction>
            <IconButton aria-label="Comments">
                <NavigateNext />
            </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Avatar>
            <DirectionsRunIcon />
          </Avatar>
          <ListItemText primary="4° Corri nel Parco" secondary="July 20, 2018" />
          <ListItemSecondaryAction>
            <IconButton aria-label="Comments">
                <NavigateNext />
            </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Avatar>
            <DirectionsRunIcon />
          </Avatar>
          <ListItemText primary="New York Marathon" secondary="Jan 9, 2018" />
          <ListItemSecondaryAction>
            <IconButton aria-label="Comments">
                <NavigateNext />
            </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Avatar>
            <DirectionsRunIcon />
          </Avatar>
          <ListItemText primary="Rome Half Marathon" secondary="Jan 7, 2018" />
          <ListItemSecondaryAction>
            <IconButton aria-label="Comments">
                <NavigateNext />
            </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Avatar>
            <DirectionsRunIcon />
          </Avatar>
          <ListItemText primary="4° Corri nel Parco" secondary="July 20, 2018" />
          <ListItemSecondaryAction>
            <IconButton aria-label="Comments">
                <NavigateNext />
            </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Avatar>
            <DirectionsRunIcon />
          </Avatar>
          <ListItemText primary="New York Marathon" secondary="Jan 9, 2018" />
          <ListItemSecondaryAction>
            <IconButton aria-label="Comments">
                <NavigateNext />
            </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
      </List>
    </div>
  );
}

FolderList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FolderList);