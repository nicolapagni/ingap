import React, { Component } from 'react';

import NavigationBar from './NavigationBar';
import Content from './Content';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavigationBar />
        <Content />
      </div>
    );
  }
}

export default App;
